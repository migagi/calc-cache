package calculator_deployment

import (
  k "kumori.systems/kumori/kmv"
  c "kumori.systems/examples/calculator/service_cache_tags:calculator"
)

#Manifest: k.#MakeDeployment & {

  _params: {
    ref: {
      domain: "kumori.systems.examples"
      name: "calccachecfg"
      version: [0,0,3]
    }

    inservice: c.#Manifest & {
      description: role: frontend: rsize: $_instances: 1
      description: role: cache:    rsize: $_instances: 1
      description: role: worker:   rsize: $_instances: 3
    }

    config: {
      parameter: {
        frontend: {
          config: {
            param_one : "myparam_one"
            param_two : 123
          }
          calculatorEnv: "The_calculator_env_value"
          restapiclientPortEnv: "80"
        }
        cache: {
          restapiclientPortEnv: "80"
        }
      }
    }
  }
}
