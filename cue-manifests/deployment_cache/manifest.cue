package calculator_deployment

import (
  k "kumori.systems/kumori/kmv"
  c "kumori.systems/examples/calculator/service_cache:calculator"
)

#Manifest: k.#MakeDeployment & {

  _params: {
    ref: {
      domain: "kumori.systems.examples"
      name: "calccachecfg"
      version: [0,0,2]
    }

    inservice: c.#Manifest & {
      description: role: worker: rsize: $_instances: 3
      description: role: cache: rsize: $_instances: 1
      description: role: frontend: rsize: $_instances: 1
    }

    config: {
      parameter: {
        frontend: {
          config: {
            param_one : "myparam_one_other"
            param_two : 456
          }
          calculatorEnv: "The_calculator_env_value_other"
          restapiclientPortEnv: "80"
        }
        cache: {
          restapiclientPortEnv: "80"
        }
      }
    }
  }
}
