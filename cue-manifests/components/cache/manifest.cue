package cache

import k "kumori.systems/kumori/kmv"

#Manifest : k.#ComponentManifest & {

  ref: {
    domain: "kumori.systems.examples"
    name: "calccachecache"
    version: [0,0,1]
  }

  description: {

    srv: {
      server: restapiserver: {
        protocol: "http"
        port:     8080
      }
      client: restapiclient: {
        protocol: "http"
      }
    }

    config: {
      resource: {}
      parameter: {
        restapiclientPortEnv: string | *"80"
      }
    }

    size: {
      $_memory: *"100Mi" | uint
      $_cpu: *"100m" | uint
    }

    code: cache: k.#Container & {
      name: "cache"
      image: {
        hub: {
          name: "registry.hub.docker.com"
          secret: ""
        }
        tag: "kumori/calccachecache:0.0.3"
      }
      mapping: {
        filesystem: []
        env: {
          RESTAPICLIENT_PORT_ENV: config.parameter.restapiclientPortEnv
          RESTAPISERVER_PORT_ENV: "\(srv.server.restapiserver.port)"
        }
      }
    }
  }
}
