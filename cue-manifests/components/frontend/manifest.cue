package frontend

import k "kumori.systems/kumori/kmv"

#Manifest : k.#ComponentManifest & {

  ref: {
    domain: "kumori.systems.examples"
    name: "calccachefe"
    version: [0,0,1]
  }

  description: {

    srv: {
      server: entrypoint: {
        protocol: "http"
        port:     8080
      }
      client: restapiclient: {
        protocol: "http"
      }
    }

    config: {
      resource: {}
      parameter: {
        config: {
          param_one : string | *"default_param_one"
          param_two : number | *"default_param_two"
        }
        calculatorEnv: string | *"default_calculatorEnv"
        restapiclientPortEnv: string | *"80"
      }
    }

    size: {
      $_memory: *"100Mi" | uint
      $_cpu: *"100m" | uint
    }

    code: frontend: k.#Container & {
      name: "frontend"
      image: {
        hub: {
          name: "registry.hub.docker.com"
          secret: ""
        }
        tag: "kumori/calccachefrontend:0.0.10"
      }
      mapping: {
        filesystem: [
          {
            path: "/config/config.json"
            data: config.parameter.config
            format: "json"
          },
        ]
        env: {
          CALCULATOR_ENV: config.parameter.calculatorEnv
          RESTAPICLIENT_PORT_ENV: config.parameter.restapiclientPortEnv
          SERVER_PORT_ENV: "\(srv.server.entrypoint.port)"
        }
      }
    }
  }
}
