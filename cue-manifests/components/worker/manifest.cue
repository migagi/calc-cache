package worker

import k "kumori.systems/kumori/kmv"

#Manifest:  k.#ComponentManifest & {

  ref: {
    domain: "kumori.systems.examples"
    name: "calccacheworker"
    version: [0,0,1]
  }

  description: {

    srv: {
      server: restapiserver: {
        protocol: "http"
        port:     8080
      }
      duplex: hello: {
        protocol: "tcp"
        port:     8081
      }
    }

    config: {
      resource: {}
      parameter: {}
    }

    size: {
      $_memory: *"100Mi" | uint
      $_cpu: *"100m" | uint
    }

    code: worker: k.#Container & {
      name: "worker"
      image: {
        hub: {
          name: "registry.hub.docker.com"
          secret: ""
        }
        tag: "kumori/calccacheworker:0.0.3"
      }
      mapping: {
        filesystem: []
        env: {
          RESTAPISERVER_PORT_ENV: "\(srv.server.restapiserver.port)"
          HELLOSERVER_PORT_ENV: "\(srv.duplex.hello.port)"
        }
      }
    }
  }
}