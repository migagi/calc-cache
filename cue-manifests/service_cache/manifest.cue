package calculator

import (
  k         "kumori.systems/kumori/kmv"
  frontend  "kumori.systems/examples/calculator/components/frontend"
  cache     "kumori.systems/examples/calculator/components/cache"
  worker   "kumori.systems/examples/calculator/components/worker"
)

let mefrontend = frontend.#Manifest
let mecache = cache.#Manifest
let meworker = worker.#Manifest

#Manifest: k.#ServiceManifest & {

  ref: {
    domain: "kumori.systems.examples"
    name: "calccache"
    version: [0,0,2]
  }

  description: {

    srv: {
      server: {
        service: {
          protocol: "http"
          port: 80
        }
      }
    }

    config: {
      parameter: {
        frontend  : mefrontend.description.config.parameter
        cache     : mecache.description.config.parameter
        worker  : meworker.description.config.parameter
      }
      resource: {}
    }

    // Config spread
    role: {
      frontend: k.#Role
      frontend: artifact: mefrontend
      frontend: cfg: parameter: config.parameter.frontend

      cache: k.#Role
      cache: artifact: mecache
      cache: cfg: parameter: config.parameter.cache

      worker: k.#Role
      worker: artifact: meworker
      worker: cfg: parameter: config.parameter.worker
    }

    connector: {
      serviceconnector: {kind: "lb"}
      lbconnector:      {kind: "lb"}
      lbconnector2:     {kind: "lb"}
      fullconnector:    {kind: "full"}
    }

    link: {

      // Outside -> FrontEnd (LB connector)
			self: service: to: "serviceconnector"
      serviceconnector: to: frontend: "entrypoint"

      // FrontEnd -> Cache (LB connector)
      frontend: restapiclient: to: "lbconnector"
      lbconnector: to: cache: "restapiserver"

      // Cache -> Worker (LB connector)
      cache: restapiclient: to: "lbconnector2"
      lbconnector2: to: worker: "restapiserver"

      // Worker -> Worker (Fullconnector)
      fullconnector: to: worker: "hello"
   }
  }
}