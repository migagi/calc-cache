package calculator

import (
  k         "kumori.systems/kumori/kmv"
  frontend  "kumori.systems/examples/calculator/components/frontend"
  worker    "kumori.systems/examples/calculator/components/worker"
)

let mefrontend = frontend.#Manifest
let meworker = worker.#Manifest

#Manifest: k.#ServiceManifest & {

  ref: {
    domain: "kumori.systems.examples"
    name: "calccache"
    version: [0,0,1]
  }

  description: {

    srv: {
      server: {
        service: {
          protocol: "http"
          port: 80
        }
      }
    }

    config: {
      parameter: {
        frontend  : mefrontend.description.config.parameter
        worker    : meworker.description.config.parameter
      }
      resource: mefrontend.description.config.resource
    }

    // Config spread
    role: {
      frontend: k.#Role
      frontend: artifact: mefrontend
      frontend: cfg: parameter: config.parameter.frontend

      worker: k.#Role
      worker: artifact: meworker
      worker: cfg: parameter: config.parameter.worker
    }

    connector: {
      serviceconnector: {kind: "lb"}
      lbconnector:      {kind: "lb"}
      fullconnector:    {kind: "full"}
    }

    link: {

      // Outside -> FrontEnd (LB connector)
			self: service: to: "serviceconnector"
      serviceconnector: to: frontend: "entrypoint"

      // FrontEnd -> Worker (LB connector)
      frontend: restapiclient: to: "lbconnector"
      lbconnector: to: worker: "restapiserver"

      // Worker -> Worker (Fullconnector)
      fullconnector: to: worker: "hello"
   }
  }
}